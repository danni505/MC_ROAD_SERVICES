# 制卡通路子域


# 应用规划

 | 服务ID |端口 | 服务名称|描述 |
|--------|-------|-------|-------|
| mc-road-parent   | xx | xx |父工程|
| mc-road-config   | 8091 | 制卡通路域配置服务 | 节点信息配置、查询服务|
| mc-road-core   | 8092|制卡通路域核心服务|节点执行核心服务|
| mc-road-retry-monit |  8093|制卡通路域执行监控服务 |节点执行日志监控、执行重试服务 |
| mc-road-route-contrl |  8094|制卡通路域路由服务|节点选择控制服务 |

# 服务
| 服务ID |端口 | 描述 |
|--------|-------|-------|
| mc-road-eureka   | xx | 注册服务|

# 测试url
http://localhost.xxx.com:8094/mcRoadRoute/simpleGetRoadId?crdOrdNbr=20180812120451474251

http://localhost.xxx.com:8092/mcRoadExec/simpleExec?crdOrdNbr=20180812120451474251

# 数据库信息
st： jdbc:mysql://99.13.252.123,99.13.253.49:3306?characterEncoding=UTF-8&user=Mcrm01exec&password=Mcrm01exec

uat：jdbc:mysql://99.13.201.42,99.13.202.18:3306?characterEncoding=UTF-8&user=Mcrm01exec&password=Mcrm01exec

# 表结构定义

详见本目录下文件《建表语句.SQL》

# 特性
1、支持同步阻塞式执行；

2、支持流式异步执行；

# TODo
1、支持流式异步节点等待及合并执行；